package main

import (
	"time"

	tarantool "github.com/tarantool/go-tarantool"
)

// NewTarantoolConn creates new tarantool connection
func NewTarantoolConn(user, address string) *tarantool.Connection {
	opts := tarantool.Opts{User: user, Reconnect: 10 * time.Second, MaxReconnects: 20}
	conn, err := tarantool.Connect(address, opts)
	if err != nil {
		panic(err)
	}
	return conn
}
