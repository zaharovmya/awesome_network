package main

type User struct {
	ID        int    `db:"id"`
	Email     string `db:"email"`
	Password  string `db:"password"`
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
	Age       int    `db:"age"`
	Sex       string `db:"sex"`
	Interests string `db:"interests"`
	City      string `db:"city"`
}
