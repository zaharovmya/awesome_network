module example.com/hello

go 1.14

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware/v2 v2.0.0-rc.2
	github.com/jmoiron/sqlx v1.3.1
	github.com/joho/godotenv v1.3.0
	github.com/opentracing-contrib/go-gin v0.0.0-20201220185307-1dd2273433a4
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/common v0.19.0
	github.com/segmentio/kafka-go v0.4.12
	github.com/tarantool/go-tarantool v0.0.0-20201220111423-77ce7d9a407a
	github.com/uber/jaeger-client-go v2.25.0+incompatible
	github.com/uber/jaeger-lib v2.4.0+incompatible
	gitlab.com/zaharovmya/network-chat v0.0.0-20210318215310-58ad3baf9614
	golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670
	google.golang.org/grpc v1.36.0
)
