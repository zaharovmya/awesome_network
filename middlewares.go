package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// AuthRequired is a simple middleware to check the session
func AuthRequired(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(userkey)
	if user == nil {
		// Abort the request with the appropriate error code
		c.Redirect(http.StatusMovedPermanently, "/user/login")
		c.Abort()
	}
	// Continue down the chain to handler etc
	c.Next()
}

func RedirectIfAuthed(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(userkey)
	if user != nil {
		c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%d", user.(int)))
		c.Abort()
	}
	// Continue down the chain to handler etc
	c.Next()
}
