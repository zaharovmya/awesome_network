package main

import (
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	chat_api "gitlab.com/zaharovmya/network-chat/client/chat"
	"google.golang.org/grpc"

	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/tracing"
	"github.com/opentracing-contrib/go-gin/ginhttp"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics"
)

const (
	userkey = "user"
)

func main() {
	tracer, closer, err := newTracer()
	if err != nil {
		log.Fatalf("Failed to create tracer: %v", err)
	}
	defer closer.Close()

	if err := engine(tracer).Run(); err != nil {
		log.Fatal("Unable to start:", err)
	}
}

func newTracer() (opentracing.Tracer, io.Closer, error) {
	cfg := jaegercfg.Configuration{
		ServiceName: "otus-network-core",
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
		},
	}

	jLogger := jaegerlog.StdLogger
	jMetricsFactory := metrics.NullFactory

	tracer, closer, err := cfg.NewTracer(
		jaegercfg.Logger(jLogger),
		jaegercfg.Metrics(jMetricsFactory),
	)

	opentracing.SetGlobalTracer(tracer)

	return tracer, closer, err
}

func engine(tracer opentracing.Tracer) *gin.Engine {

	godotenv.Load(".env")
	r := gin.New()
	r.Use(ginhttp.Middleware(tracer))
	r.LoadHTMLGlob("templates/*")
	r.Use(sessions.Sessions("mysession", sessions.NewCookieStore([]byte("secret"))))

	dep := newDependences()

	user := r.Group("/user")
	user.Use(RedirectIfAuthed)
	{
		user.GET("/login", getLoginPage)
		user.POST("/login", dep.login)
		user.GET("/register", getRegistrationPage)
		user.POST("/register", dep.register)
	}

	r.GET("/logout", logout)
	r.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/user/login")
		c.Abort()
	})

	private := r.Group("/profile")
	private.Use(AuthRequired)
	{
		private.GET("/:profileid", dep.getProfile)
		private.GET("/:profileid/add_friend", dep.addFriend)
		private.GET("/:profileid/news", dep.getNewsFeedPage)
		private.POST("/:profileid/message", dep.addMessage)
	}

	news := r.Group("/news")
	news.Use(AuthRequired)
	{
		news.POST("/", dep.publishNews)
	}

	search := r.Group("/search")
	search.Use(AuthRequired)
	{
		search.GET("/", getSearchPage)
		search.POST("/", dep.searchProfiles)
	}
	return r
}

func newDependences() *Dependences {
	dbParams := &DBConnectionParams{
		UserName: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		URI:      os.Getenv("DB_URI"),
		Port:     os.Getenv("DB_PORT"),
		DBName:   os.Getenv("DB_NAME"),
	}
	dbConn := NewDBConn(dbParams)
	kafkaURL := []string{os.Getenv("KAFKA_URL")}
	eventSender := NewEventSender(kafkaURL, os.Getenv("KAFKA_CLIENT_ID"), os.Getenv("KAFKA_TOPIC"))
	tarantoolConn := NewTarantoolConn(os.Getenv("TARANTOOL_NAME"), os.Getenv("TARANTOOL_URL"))
	chatClient, err := NewChatClient(os.Getenv("CHAT_SERVICE_URL"))
	if err != nil {
		log.Fatal("Unable to start:", err)
	}
	return &Dependences{
		DBConn:        dbConn,
		EventSender:   eventSender,
		TarantoolConn: tarantoolConn,
		ChatClient:    chatClient,
	}
}

func NewGRPCConn(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts,
		grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor(
			grpc_opentracing.WithTracer(opentracing.GlobalTracer()),
		)),
	)
	opts = append(opts,
		grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor(
			grpc_opentracing.WithTracer(opentracing.GlobalTracer()),
		)),
	)
	opts = append(opts, grpc.WithInsecure(), grpc.WithBlock())
	return grpc.Dial(address, opts...)
}

func NewChatClient(address string) (chat_api.ChatServiceClient, error) {
	conn, err := NewGRPCConn(address)
	if err != nil {
		return nil, err
	}
	client := chat_api.NewChatServiceClient(conn)
	return client, nil
}
