package main

import (
	"context"
	"encoding/json"
	"time"

	"github.com/segmentio/kafka-go"
)

type Event struct {
	UserID    int    `json:"id"`
	Name      string `json:"type"`
	Text      string `json:"text"`
	Timestamp int64  `json:"timestamp"`
}

type EventSender struct {
	Writer *kafka.Writer
}

func NewEventSender(kafkaBrokerUrls []string, clientID, topic string) *EventSender {
	dialer := &kafka.Dialer{
		Timeout:  10 * time.Second,
		ClientID: clientID,
	}

	config := kafka.WriterConfig{
		Brokers:      kafkaBrokerUrls,
		Topic:        topic,
		Balancer:     &kafka.LeastBytes{},
		Dialer:       dialer,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}

	return &EventSender{Writer: kafka.NewWriter(config)}
}

func (es *EventSender) Publish(ctx context.Context, event *Event) error {

	body, _ := json.Marshal(event)

	return es.Writer.WriteMessages(ctx, kafka.Message{Value: []byte(body)})
}
