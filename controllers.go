package main

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/common/log"
	tarantool "github.com/tarantool/go-tarantool"
	chat_api "gitlab.com/zaharovmya/network-chat/client/chat"
	"golang.org/x/crypto/bcrypt"
)

type Dependences struct {
	DBConn        *sqlx.DB
	EventSender   *EventSender
	TarantoolConn *tarantool.Connection
	ChatClient    chat_api.ChatServiceClient
}

type Login struct {
	Email    string `form:"email" binding:"required"`
	Password string `form:"password" binding:"required"`
}

type SearchProfileForm struct {
	FirstName string `form:"first_name" binding:"required"`
	LastName  string `form:"last_name" binding:"required"`
}

type PublishNewsForm struct {
	Message string `form:"message" binding:"required"`
}

type Registration struct {
	Email     string `form:"email" binding:"required"`
	Password  string `form:"password" binding:"required"`
	FirstName string `form:"first_name" binding:"required"`
	LastName  string `form:"last_name" binding:"required"`
	Sex       string `form:"sex" binding:"required"`
	Age       int    `form:"age" binding:"required"`
	Interests string `form:"interests" binding:"required"`
	City      string `form:"city" binding:"required"`
}

func getLoginPage(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", gin.H{})
}

// login is a handler that parses a form and checks for specific data
func (d *Dependences) login(c *gin.Context) {
	session := sessions.Default(c)

	var form Login

	if err := c.MustBindWith(&form, binding.Form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user User

	err := d.DBConn.Get(&user, "select * from users where email=?", form.Email)

	if err == sql.ErrNoRows {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(form.Password))
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
		return
	}

	// Save the username in the session
	session.Set(userkey, user.ID)
	if err := session.Save(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save session"})
		return
	}
	c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%d", user.ID))
	c.Abort()
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get(userkey)
	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid session token"})
		return
	}

	session.Clear()
	session.Save()

	c.Redirect(http.StatusTemporaryRedirect, "/")
	c.Abort()
}

func getRegistrationPage(c *gin.Context) {
	c.HTML(http.StatusOK, "register.html", gin.H{})
}

func hashAndSalt(password string) string {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	return string(hash)
}

func (d *Dependences) register(c *gin.Context) {
	session := sessions.Default(c)

	var form Registration

	if err := c.MustBindWith(&form, binding.Form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user User

	err := d.DBConn.Get(&user, "select * from users where email=?", form.Email)

	if err == nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User already exists"})
		return
	}

	var query = "INSERT INTO users (`email`, `first_name`, `last_name`, `age`, `sex`, `interests`, `city`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"

	hashPassword := hashAndSalt(form.Password)
	result, err := d.DBConn.Exec(query, form.Email, form.FirstName, form.LastName, form.Age, form.Sex, form.Interests, form.City, hashPassword)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	id, _ := result.LastInsertId()

	session.Set(userkey, int(id))
	if err := session.Save(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save session"})
		return
	}
	c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%d", id))
	c.Abort()
}

func (d *Dependences) getProfile(c *gin.Context) {
	session := sessions.Default(c)
	userID := session.Get(userkey)

	profileID := c.Param("profileid")

	var user User

	err := d.DBConn.Get(&user, "select * from users where id=?", profileID)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}

	friends, err := d.getFriends(profileID)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	chatId := d.chatId(strconv.Itoa(userID.(int)), profileID)

	request := &chat_api.GetMessagesRequest{
		ChatId: chatId,
	}

	resp, err := d.ChatClient.GetMessages(c.Request.Context(), request)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.HTML(http.StatusOK, "profile.tmpl", gin.H{"user": &user, "userId": userID.(int), "friends": &friends, "messages": &resp.Messages})

}

func (d *Dependences) chatId(userID, profileID string) string {
	s := []string{userID, profileID}
	sort.Strings(s)
	return s[0] + "-" + s[1]
}

func (d *Dependences) sendPublishNewsEvent(userID int, message string) {

	friends, _ := d.getFriends(strconv.Itoa(userID))
	for _, friend := range friends {
		event := &Event{
			Name:      "PublishNews",
			UserID:    friend.ID,
			Text:      message,
			Timestamp: time.Now().Unix(),
		}

		d.EventSender.Publish(context.TODO(), event)
	}
}

func (d *Dependences) getFriends(profileID string) (f []User, err error) {
	friends := []User{}

	err = d.DBConn.Select(&friends, "SELECT users.* FROM friends JOIN users ON users.id=friends.friend_id AND friends.user_id=? UNION SELECT users.* FROM friends JOIN users ON users.id=friends.user_id AND friends.friend_id=?", profileID, profileID)

	return friends, err

}

func (d *Dependences) addFriend(c *gin.Context) {
	session := sessions.Default(c)
	userID := session.Get(userkey)

	friendID, _ := strconv.Atoi(c.Param("profileid"))

	var query = "INSERT INTO friends (`user_id`, `friend_id`, `state`) VALUES (?, ?, ?)"
	_, err := d.DBConn.Exec(query, userID, friendID, 0)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%d", friendID))
	c.Abort()
}

func (d *Dependences) publishNews(c *gin.Context) {
	session := sessions.Default(c)
	userID := session.Get(userkey)

	var form PublishNewsForm

	if err := c.MustBindWith(&form, binding.Form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	go d.sendPublishNewsEvent(userID.(int), form.Message)

	c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%d", userID))
	c.Abort()
}

func getSearchPage(c *gin.Context) {
	c.HTML(http.StatusOK, "search.tmpl", gin.H{})
}

func (d *Dependences) searchProfiles(c *gin.Context) {
	var form SearchProfileForm

	if err := c.MustBindWith(&form, binding.Form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	users := []User{}

	err := d.DBConn.Select(&users, "select * from users where last_name LIKE ? AND first_name LIKE ?", form.LastName+"%", form.FirstName+"%")

	if err == sql.ErrNoRows {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "DB error"})
		return
	}

	c.HTML(http.StatusOK, "search.tmpl", gin.H{"users": &users})

}

type NewsFeed struct {
	ID        string
	Timestamp int64
	Text      string
}

func (d *Dependences) getNewsFeedPage(c *gin.Context) {

	profileID, _ := strconv.Atoi(c.Param("profileid"))

	var news []NewsFeed

	err := d.TarantoolConn.SelectTyped(profileID, 1, 0, 1000, tarantool.IterLe, []interface{}{time.Now().Unix()}, &news)
	if err != nil {

		if !strings.Contains(err.Error(), "there is no space with") {
			log.Error(err)
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

	}

	c.HTML(http.StatusOK, "news_feed.tmpl", gin.H{"news": news})

}

type MessageForm struct {
	Msg string `form:"msg" binding:"required"`
}

func (d *Dependences) addMessage(c *gin.Context) {
	var form MessageForm

	if err := c.MustBindWith(&form, binding.Form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	session := sessions.Default(c)

	userID := session.Get(userkey)

	friendID := c.Param("profileid")

	chatID := d.chatId(strconv.Itoa(userID.(int)), friendID)

	request := &chat_api.AddMessageRequest{
		ChatId:  chatID,
		UserId:  int64(userID.(int)),
		Message: form.Msg,
	}
	_, err := d.ChatClient.AddMessage(c.Request.Context(), request)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("/profile/%s", friendID))
	c.Abort()
}
